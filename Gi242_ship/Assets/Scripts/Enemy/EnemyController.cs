﻿using UnityEngine;
using Spaceship;
using Manager;

namespace Enemy
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;

        private PlayerSpaceship spawnedPlayerShip;

        //public void Init(PlayerSpaceship playerSpaceship)
        //{
        //    spawnedPlayerShip = playerSpaceship;
        //}
        private void Awake()
        {
            spawnedPlayerShip = GameManager.Instance.spawnedPlayerShip;
        }

        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

         private void MoveToPlayer()
         {
            // TODO: Implement this later
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);
            if (distanceToPlayer < chasingThresholdDistance)
            {
                var direction = (Vector2)(spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
         }
    }    
}