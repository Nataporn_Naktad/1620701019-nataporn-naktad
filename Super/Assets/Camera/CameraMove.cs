﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] private Transform target = null;
    private Vector3 offset;
    public float smoothSpeed = 0.125f;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - target.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.Lerp(transform.position,
            new Vector3(target.position.x, 0 , target.position.z) + offset, Time.deltaTime * 3);
        
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothPosition = Vector3.Lerp (transform.position , desiredPosition , smoothSpeed);
        transform.position = smoothPosition;

        transform.LookAt(target);
    }
}
