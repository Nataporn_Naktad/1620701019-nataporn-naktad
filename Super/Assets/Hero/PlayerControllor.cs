﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllor : MonoBehaviour
{
    public Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            GetComponent<Rigidbody>().AddTorque(x: 0, y: 0, z: -10);
        }

        if (Input.GetKey(KeyCode.S))
        {
            GetComponent<Rigidbody>().AddTorque(x: 0, y: 0, z: 10);
        }

        if (Input.GetKey(KeyCode.A))
        {
            GetComponent<Rigidbody>().AddTorque(x: 10, y: 0, z: 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            GetComponent<Rigidbody>().AddTorque(x: -10, y: 0, z: 0);
        }

        if (Input.GetButtonDown("Jump"))
        {
            rb.AddForce(new Vector3(x: 1, y: 5, z: 0), ForceMode.Impulse);
        }
    }
}