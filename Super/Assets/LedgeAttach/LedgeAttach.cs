﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LedgeAttach : MonoBehaviour
{
   public GameObject TheLedge;
   public GameObject ThePlayer;

   private void OnTriggerEnter(Collider other)
   {
      ThePlayer.transform.parent = TheLedge.transform;
   }
}
