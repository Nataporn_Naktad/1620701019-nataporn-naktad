﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    public GameObject BridgeUp;
    
    public float maximumOpening = 10f;
    

    public float moveSpeed = 5f;
    
    private bool playerIsHere;
    private bool opening;

    private void Start()
    {
        playerIsHere = false;
        opening = false;
    }

    private void Update()
    {
        if (playerIsHere)
        {
            if (BridgeUp.transform.position.y < maximumOpening)
            {
                BridgeUp.transform.Translate(0f, moveSpeed * Time.deltaTime, 0f);
            }
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerIsHere = true;
        }
    }
    
    private void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            playerIsHere = false;
        }
    }
    
}
