﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Rotation : MonoBehaviour
{
    public Rigidbody rb;
    [SerializeField] private Vector3 angularVelocity;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(new Vector3(0f,0.25f,0f));
        
    }
    
    
}
