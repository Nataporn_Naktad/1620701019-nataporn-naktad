﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationS1 : MonoBehaviour
{
    public Rigidbody rb;
    [SerializeField] private Vector3 angularVelocity;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        transform.Rotate(new Vector3(1f,3f,1f));
        
    }
}
